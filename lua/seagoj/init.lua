require('seagoj.filetypes').config()
require('seagoj.options').defaults()
require('seagoj.options').gui()
require('seagoj.mappings')
require('seagoj.abbreviations')
require('seagoj.plugins').ui()
require('seagoj.plugins').diagnostics()
require('seagoj.plugins').telescope()
require('seagoj.plugins').completions()
require('seagoj.plugins').notes()
require('seagoj.plugins').misc()

-- AUTOCMDS
local autocmd = vim.api.nvim_create_autocmd
local augroup = vim.api.nvim_create_augroup

autocmd('TextYankPost', {
  group = augroup('HighlightYank', {}),
  pattern = '*',
  callback = function()
    vim.highlight.on_yank({
      higroup = 'IncSearch',
      timeout = 200
    })
  end
})

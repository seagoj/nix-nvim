-- vim: set expandtab:

-- require('seagoj.pack').pack {
--   'plenary.nvim',
--   'telescope.nvim',
--   'nvim-web-devicons',
--   'alpha-nvim',
--   --  'neovim-session-manager'
-- }

local dashboard = require('alpha.themes.dashboard')
math.randomseed(os.time())

local function pick_color()
  local colors = { "String", "Identifier", "Keyword", "Number" }
  return colors[math.random(#colors)]
end

local function button(sc, txt, keybind, keybind_opts)
  local b = dashboard.button(sc, txt, keybind, keybind_opts)
  b.opts.hl = "Function"
  b.opts.hl_shortcut = "Type"
  return b
end

local function footer()
  local datetime = os.date("%Y-%m-%d  %H:%M:%S")

  return { datetime }
end

-- generated with `figlet -f shadow "Hack the Planet"`
dashboard.section.header.val = {
  [[ |   |            |        |   |               _ \  |                  |   ]],
  [[ |   |  _` |  __| |  /     __| __ \   _ \     |   | |  _` | __ \   _ \ __| ]],
  [[ ___ | (   | (      <      |   | | |  __/     ___/  | (   | |   |  __/ |   ]],
  [[_|  _|\__,_|\___|_|\_\    \__|_| |_|\___|    _|    _|\__,_|_|  _|\___|\__| ]]
}
dashboard.section.header.opts.hl = pick_color()

dashboard.section.buttons.val = {
  button('e', ' New file', '<Cmd>Alpha<CR>'),
  button('<leader> p', ' Find file'),
  button('<leader> f h', ' Recently opened files'),
  button('<leader> /', ' Search Project'),
  --  button('<leader> s l', ' Open session'),
  button('q', ' Quit', '<Cmd>qa<CR>'),
}

dashboard.section.footer.val = footer()
dashboard.section.footer.opts.hl = "Constant"

require 'alpha'.setup(dashboard.opts)

local Path = require('plenary.path')
-- require('session_manager').setup({
-- sessions_dir = Path:new(vim.fn.stdpath('data'), 'sessions'),
-- path_replacer = '__',
-- colon_replacer = '++',
-- autoload_mode = false,-- require('session_manager.config').AutoloadMode.LastSession,
-- autosave_last_session = false,
-- autosave_ignore_not_normal = false,
-- })



vim.keymap.set({ 'n' }, "<leader>fh", ":Telescope oldfiles<CR>")
-- vim.keymap.set({'n'}, "<leader>sl", ":Telescope sessions<CR>")
-- vim.keymap.set({'n'}, "<leader>ss", ":SaveSession<CR>")

-- require('telescope').load_extension('sessions')

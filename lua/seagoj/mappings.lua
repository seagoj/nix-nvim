-- We use which-key in mappings, which is loaded before plugins, so set up here
local which_key = require("which-key")
which_key.setup({
  plugins = {
    marks = true,      -- shows a list of your marks on ' and `
    registers = true,  -- shows your registers on " in NORMAL or <C-r> in INSERT mode
    spelling = {
      enabled = true,  -- enabling this will show WhichKey when pressing z= to select spelling suggestions
      suggestions = 20 -- how many suggestions should be shown in the list?
    },
    -- the presets plugin, adds help for a bunch of default keybindings in Neovim
    -- No actual key bindings are created
    presets = {
      operators = true,    -- adds help for operators like d, y, ... and registers them for motion / text object completion
      motions = false,     -- adds help for motions
      text_objects = true, -- help for text objects triggered after entering an operator
      windows = true,      -- default bindings on <c-w>
      nav = true,          -- misc bindings to work with windows
      z = true,            -- bindings for folds, spelling and others prefixed with z
      g = true             -- bindings for prefixed with g
    }
  },
  icons = {
    breadcrumb = "»", -- symbol used in the command line area that shows your active key combo
    separator = "➜", -- symbol used between a key and it's label
    group = "+"       -- symbol prepended to a group
  },
  popup_mappings = {
    scroll_down = "<c-d>", -- binding to scroll down inside the popup
    scroll_up = "<c-u>"    -- binding to scroll up inside the popup
  },
  window = {
    border = "rounded",       -- none, single, double, shadow
    position = "bottom",      -- bottom, top
    margin = { 1, 0, 1, 0 },  -- extra window margin [top, right, bottom, left]
    padding = { 2, 2, 2, 2 }, -- extra window padding [top, right, bottom, left]
    winblend = 0
  },
  layout = {
    height = { min = 4, max = 25 }, -- min and max height of the columns
    width = { min = 20, max = 50 }, -- min and max width of the columns
    spacing = 3,                    -- spacing between columns
    align = "left"                  -- align columns left, center or right
  },
  ignore_missing = true,            -- enable this to hide mappings for which you didn't specify a label
  hidden = {
    "<silent>", "<CMD>", "<cmd>", "<Cmd>", "<cr>", "<CR>", "call", "lua",
    "^:", "^ "
  },                -- hide mapping boilerplate
  show_help = true, -- show help message on the command line when the popup is visible
  triggers = "auto" -- automatically setup triggers
  -- triggers = {"<leader>"} -- or specify a list manually
})
-- This file is for mappings that will work regardless of filetype. Always available.
local options = { noremap = true, silent = true }
local all_modes = { 'n', 'v', 'i', 'x', '!' }
local read_modes = { 'n', 'v' }

-- Make F1 act like escape for accidental hits
vim.keymap.set(all_modes, '<F1>', '<Esc>', options)

-- Clear search highlighting on a redraw
vim.keymap.set(read_modes, '<leader><space>', '<cmd>nohlsearch<CR><C-l>', { silent = true })

-- Yank to end of line using more familiar method
vim.keymap.set(read_modes, 'Y', 'y$', options)

local leader_opts = {
  -- prefix = "<leader>",
  buffer = nil,   -- Global mappings. Specify a buffer number for buffer local mappings
  silent = true,  -- use `silent` when creating keymaps
  noremap = true, -- use `noremap` when creating keymaps
  nowait = true   -- use `nowait` when creating keymaps
}

-- copy/paste to clipboard
vim.keymap.set({ "v" }, '<leader>c', '"+y', leader_opts)
vim.keymap.set({ "v" }, '<leader>v', '"+p', leader_opts)

-- change directory to file in current buffer
vim.keymap.set({ 'n' }, '<leader>cd', '<cmd>cd %:p:h<CR>', leader_opts)
-- make directory for file in current buffer
vim.keymap.set({ 'n' }, '<leader>md', '<cmd>!mkdir -p %:p:h<CR>', leader_opts)

-- make buffers equal size
vim.keymap.set({ 'n' }, '<leader>=', '<C-w>=', leader_opts)
-- splits
vim.keymap.set({ 'n' }, '<leader>-', '<cmd>new<CR>', leader_opts)
vim.keymap.set({ 'n' }, '<leader>|', '<cmd>vnew<CR>', leader_opts)

-- Files
vim.keymap.set(read_modes, '<leader>e', '<cmd>NvimTreeToggle<CR>', leader_opts)
vim.keymap.set({ 'n' }, '<leader>o', '<Esc><cmd>Telescope git_files<CR>', leader_opts)
vim.keymap.set({ 'n' }, '<leader>p', '<Esc><cmd>Telescope find_files<CR>', leader_opts)
vim.keymap.set({ 'n' }, '<leader>/', [[<Esc><cmd>Telescope live_grep<CR>]], leader_opts)
vim.keymap.set(read_modes, '<leader>x', '<cmd>Bdelete!<CR>', leader_opts)
vim.keymap.set(read_modes, '<leader>fb',
  [[<cmd>lua require('telescope.builtin').buffers(require('telescope.themes').get_dropdown{previewer = false})<cr>]],
  leader_opts)
vim.keymap.set(read_modes, '<leader>fh', [[<cmd>lua require('telescope.builtin').oldfiles()<cr>]], leader_opts)
vim.keymap.set(read_modes, '<leader>fq', [[<cmd>lua require('telescope.builtin').quickfix()<cr>]], leader_opts)
vim.keymap.set(read_modes, '<leader>fl', [[<cmd>lua require('telescope.builtin').loclist()<cr>]], leader_opts)
vim.keymap.set(read_modes, '<leader>fp', '<cmd>Telescope projects<cr>', leader_opts)
vim.keymap.set(read_modes, '<leader>fk', '<cmd>Telescope keymaps<cr>', leader_opts)
vim.keymap.set(read_modes, '<leader>ft',
  [[<cmd>lua require('telescope.builtin').grep_string{search = \"^\\\\s*[*-] \\\\[ \\\\]\", previewer = false, glob_pattern = \"*.md\", use_regex = true, disable_coordinates=true}<cr>]],
  leader_opts)
vim.keymap.set(read_modes, '<leader>w', '<cmd>%retab!<cr>', leader_opts)

-- quickfix
vim.keymap.set(read_modes, '<leader>q',
  [["<cmd>".(get(getqflist({"winid": 1}), "winid") != 0? "cclose" : "botright copen")."<cr>"]], leader_opts)

-- git
vim.keymap.set({ 'n' }, '<leader>gs', "<cmd>lua require('telescope.builtin').git_status()<cr>", leader_opts)
vim.keymap.set({ 'n' }, '<leader>gb', "<cmd>lua require('telescope.builtin').git_branches()<cr>", leader_opts)
vim.keymap.set({ 'n' }, '<leader>gc', "<cmd>lua require('telescope.builtin').git_commits()<cr>", leader_opts)
vim.keymap.set({ 'n' }, '<leader>gh', "<cmd>lua require 'gitsigns'.toggle_current_line_blame<cr>", leader_opts)
vim.keymap.set({ 'n' }, '<leader>g-', "<cmd>lua require 'gitsigns'.reset_hunk()<cr>", leader_opts)
vim.keymap.set({ 'n' }, '<leader>g+', "<cmd>lua require 'gitsigns'.stage_hunk()<cr>", leader_opts)

-- notes
vim.keymap.set({ 'n' }, '<leader>ng', "<cmd>lua require('seagoj.plugins').grammar_check()<cr>", leader_opts)

vim.keymap.set(all_modes, '<leader>fd', '<cmd> Telescope lsp_document_symbols<CR>', leader_opts)

-- local vimrc = os.getenv('MYVIMRC');
-- vim.keymap.set(read_modes, '<leader>ev', '<cmd>tabnew ' .. vimrc .. '<CR>', leader_opts)

vim.keymap.set(read_modes, '<leader>k', '0y%:! open <C-r>"<CR>j<leader>k', {
  buffer = nil,
  silent = true,
  nowait = true
})

-- TODO replace with projectionist
-- open test
vim.keymap.set(read_modes, '<leader>tdd', '<cmd>vsplit tests/%<CR>', leader_opts)

-- Move lines
vim.keymap.set({ 'n' }, '<leader><Up>', '<cmd>m-2<CR>', leader_opts)
vim.keymap.set({ 'n' }, '<leader><Down>', '<cmd>m+1<CR>', leader_opts)

-- align paragraph
vim.keymap.set(read_modes, '<leader>ap', '=ip', leader_opts)
-- clone paragraph
vim.keymap.set(read_modes, '<leader>cp', 'yap<S-]>p', leader_opts)

-- Esc from insert on movement
vim.keymap.set({ 'i' }, 'jj', '<Esc>jj', leader_opts)
vim.keymap.set({ 'i' }, 'kk', '<Esc>kk', leader_opts)
vim.keymap.set({ 'i' }, 'jk', '<Esc>', leader_opts)

-- indent
vim.keymap.set({ 'v' }, '<', '<gv', leader_opts)
vim.keymap.set({ 'v' }, '>', '>gv', leader_opts)

-- fix ex command
vim.keymap.set({ 'n' }, ';', ':', leader_opts)

-- TODO what does this do?
vim.keymap.set({ 'n' }, 'p', ']p', leader_opts)

-- disable ex mode
vim.keymap.set({ 'n' }, 'Q', '<Nop>', leader_opts)

vim.keymap.set({ 'c' }, 'w!!', 'w !sudo tee % > /dev/null', leader_opts)

-- snake to camelCase
vim.keymap.set({ 'n' }, '<leader>cc', 'EF_x~', leader_opts)
-- camel to snake_case
vim.keymap.set({ 'n' }, '<leader>_', [[<cmd>s#\(\<\u\l\+\|\l\+\)\(\u\)#\l\1_\l\2#g<CR>]], leader_opts)

-- faster scroll
vim.keymap.set({ 'n' }, '<C-e>', '10<C-e>', leader_opts)
vim.keymap.set({ 'n' }, '<C-y>', '10<C-y>', leader_opts)
vim.keymap.set({ 'n' }, 'gV', '`[v`]', leader_opts)

-- <Tab> toggles folding
vim.keymap.set({ 'n' }, '<Tab>', [[@=(foldlevel('.')?'za':"\<Space>")<CR>]], leader_opts)

-- show current filename
vim.keymap.set({ 'n' }, '<leader>fn', '<cmd>echo @%<CR>', leader_opts)
vim.keymap.set({ 'n' }, '<leader>fp', '<cmd>let @" = expand("%:p")', leader_opts)

-- autoformat entire file
vim.keymap.set({ 'n' }, '<leader><Tab>', '<Esc>gg=G<C-o>zz', leader_opts)

-- navigate wrapped lines
vim.keymap.set(read_modes, 'j', 'gj', {
  buffer = nil,
  silent = true,
  nowait = true
})
vim.keymap.set(read_modes, 'k', 'gk', {
  buffer = nil,
  silent = true,
  nowait = true
})

-- tab management
vim.keymap.set(read_modes, '<leader>tn', '<cmd>tabnew<CR><C-p>', leader_opts)
vim.keymap.set(read_modes, '<leader>to', '<cmd>tabonly<CR>', leader_opts)
vim.keymap.set(read_modes, '<leader>tc', '<cmd>tabclose<CR>', leader_opts)
vim.keymap.set(read_modes, '<leader>tm', '<cmd>tabmove<Space>', leader_opts)

-- Debug syntax files
vim.keymap.set(all_modes, '<leader>sd', [[:echo map(synstack(line('.'), col('.')), 'synIDattr(v:val, "name")')<CR>]],
  options)

-- """"""""" Global Shortcuts """""""""""""

-- gx is a built-in to open URLs under the cursor, but when
-- not using netrw, it doesn't work right. Or maybe it's just me
-- but anyway this command works great.
-- [abc](https://github.com/adsr/mle/commit/e4dc4314b02a324701d9ae9873461d34cce041e5.patch)
vim.api.nvim_set_keymap('', 'gx',
  ":silent !open \"<c-r><c-f>\" || xdg-open \"<c-r><c-f>\"<cr>",
  options)
vim.api.nvim_set_keymap('v', 'gx',
  "\"0y:silent !open \"<c-r>0\" || xdg-open \"<c-r>0\"<cr>gv",
  options)
vim.api.nvim_set_keymap('', '<CR>',
  ":silent !open \"<c-r><c-f>\" || xdg-open \"<c-r><c-f>\"<cr>",
  options)
vim.api.nvim_set_keymap('v', '<CR>',
  "\"0y:silent !open \"<c-r>0\" || xdg-open \"<c-r>0\"<cr>gv",
  options)

-- START HERE

-- Make nvim terminal more sane
vim.api.nvim_set_keymap('t', '<Esc>', [[<C-\><C-n>]], options)
vim.api.nvim_set_keymap('t', '<M-[>', "<Esc>", options)
vim.api.nvim_set_keymap('t', '<C-v><Esc>', "<Esc>", options)

-- gui nvim stuff
-- Adjust font sizes
vim.api.nvim_set_keymap('', '<D-=>', [[:silent! let &guifont = substitute(
  \ &guifont,
  \ ':h\zs\d\+',
  \ '\=eval(submatch(0)+1)',
  \ '')<CR>]], options)
vim.api.nvim_set_keymap('', '<C-=>', [[:silent! let &guifont = substitute(
  \ &guifont,
  \ ':h\zs\d\+',
  \ '\=eval(submatch(0)+1)',
  \ '')<CR>]], options)
vim.api.nvim_set_keymap('', '<D-->', [[:silent! let &guifont = substitute(
  \ &guifont,
  \ ':h\zs\d\+',
  \ '\=eval(submatch(0)-1)',
  \ '')<CR>]], options)
vim.api.nvim_set_keymap('', '<C-->', [[:silent! let &guifont = substitute(
  \ &guifont,
  \ ':h\zs\d\+',
  \ '\=eval(submatch(0)-1)',
  \ '')<CR>]], options)

-- Need to map cmd-c and cmd-v to get natural copy/paste behavior
vim.api.nvim_set_keymap('n', '<D-v>', '"*p', options)
vim.api.nvim_set_keymap('v', '<D-v>', '"*p', options)
vim.api.nvim_set_keymap('!', '<D-v>', '<C-R>*', options)
vim.api.nvim_set_keymap('c', '<D-v>', '<C-R>*', options)
vim.api.nvim_set_keymap('v', '<D-c>', '"*y', options)
-- When pasting over selected text, keep original register value
vim.api.nvim_set_keymap('v', 'p', '"_dP', options)

-- cmd-w to close the current buffer
vim.api.nvim_set_keymap('', '<D-w>', ':bd<CR>', options)
vim.api.nvim_set_keymap('!', '<D-w>', '<ESC>:bd<CR>', options)

-- cmd-t or cmd-n to open a new buffer
vim.api.nvim_set_keymap('', '<D-t>', ':enew<CR>', options)
vim.api.nvim_set_keymap('!', '<D-t>', '<ESC>:enew<CR>', options)
vim.api.nvim_set_keymap('', '<D-n>', ':tabnew<CR>', options)
vim.api.nvim_set_keymap('!', '<D-n>', '<ESC>:tabnew<CR>', options)

-- cmd-s to save
vim.api.nvim_set_keymap('', '<D-s>', ':w<CR>', options)
vim.api.nvim_set_keymap('!', '<D-s>', '<ESC>:w<CR>', options)

-- cmd-q to quit
vim.api.nvim_set_keymap('', '<D-q>', ':q<CR>', options)
vim.api.nvim_set_keymap('!', '<D-q>', '<ESC>:q<CR>', options)

-- cmd-o to open
vim.api.nvim_set_keymap('', '<D-o>', ':Telescope file_browser cmd=%:h<CR>',
  options)
vim.api.nvim_set_keymap('!', '<D-o>',
  '<ESC>:Telescope file_browser cmd=%:h<CR>', options)

-- emacs bindings to jump around in lines
vim.api.nvim_set_keymap("i", "<C-e>", "<C-o>A", options)
vim.api.nvim_set_keymap("i", "<C-a>", "<C-o>I", options)

-- TODO:
-- Use ctrl-x, ctrl-u to complete :emoji: symbols, then use
-- ,e to turn it into a symbol if desired
-- vim.api.nvim_set_keymap('!', '<leader>e',
--                      [[:%s/:\([^:]\+\):/\=emoji#for(submatch(1), submatch(0))/g<CR>]],
--                     options)

-- Setup tpope unimpaired-like forward/backward shortcuts
which_key.register({
  ["[a"] = "Prev file arg",
  ["]a"] = "Next file arg",
  ["[b"] = { '<Cmd>BufferLineCyclePrev<CR>', "Prev buffer" },
  ["]b"] = { '<Cmd>BufferLineCycleNext<CR>', "Next buffer" },
  ["[c"] = "Prev git hunk",
  ["]c"] = "Next git hunk",
  ["[l"] = "Prev loclist item",
  ["]l"] = "Next loclist item",
  ["[q"] = "Prev quicklist item",
  ["]q"] = "Next quicklist item",
  ["[t"] = { '<Cmd>tabprevious<cr>', "Prev tab" },
  ["[T"] = { '<Cmd>tabprevious<cr>', "First tab" },
  ["]t"] = { '<Cmd>tabnext<cr>', "Next tab" },
  ["]T"] = { '<Cmd>tablast<cr>', "Last tab" },
  ["[n"] = "Prev conflict",
  ["]n"] = "Next conflict",
  ["[ "] = "Add blank line before",
  ["] "] = "Add blank line after",
  ["[e"] = "Swap line with previous",
  ["]e"] = "Swap line with next",
  ["[x"] = "XML encode",
  ["]x"] = "XML decode",
  ["[u"] = "URL encode",
  ["]u"] = "URL decode",
  ["[y"] = "C escape",
  ["]y"] = "C unescape",
  ["[d"] = { "<cmd>lua vim.diagnostic.goto_prev()<CR>", "Prev diagnostic" },
  ["]d"] = { "<cmd>lua vim.diagnostic.goto_next()<CR>", "Next diagnostic" },
  ["[1"] = { ':BufferLineGoToBuffer 1<CR>', "Go to buffer 1" },
  ["]1"] = { ':BufferLineGoToBuffer 1<CR>', "Go to buffer 1" },
  ["[2"] = { ':BufferLineGoToBuffer 2<CR>', "Go to buffer 2" },
  ["]2"] = { ':BufferLineGoToBuffer 2<CR>', "Go to buffer 2" },
  ["[3"] = { ':BufferLineGoToBuffer 3<CR>', "Go to buffer 3" },
  ["]3"] = { ':BufferLineGoToBuffer 3<CR>', "Go to buffer 3" },
  ["[4"] = { ':BufferLineGoToBuffer 4<CR>', "Go to buffer 4" },
  ["]4"] = { ':BufferLineGoToBuffer 4<CR>', "Go to buffer 4" },
  ["[5"] = { ':BufferLineGoToBuffer 5<CR>', "Go to buffer 5" },
  ["]5"] = { ':BufferLineGoToBuffer 5<CR>', "Go to buffer 5" },
  ["[6"] = { ':BufferLineGoToBuffer 6<CR>', "Go to buffer 6" },
  ["]6"] = { ':BufferLineGoToBuffer 6<CR>', "Go to buffer 6" },
  ["[7"] = { ':BufferLineGoToBuffer 7<CR>', "Go to buffer 7" },
  ["]7"] = { ':BufferLineGoToBuffer 7<CR>', "Go to buffer 7" },
  ["[8"] = { ':BufferLineGoToBuffer 8<CR>', "Go to buffer 8" },
  ["]8"] = { ':BufferLineGoToBuffer 8<CR>', "Go to buffer 8" },
  ["[9"] = { ':BufferLineGoToBuffer 9<CR>', "Go to buffer 9" },
  ["]9"] = { ':BufferLineGoToBuffer 9<CR>', "Go to buffer 9" },
  ["<S-h>"] = { ':BufferLineCyclePrev<CR>', "Go to next buffer" },
  ["<S-l>"] = { ':BufferLineCycleNext<CR>', "Go to prev buffer" },
}, { mode = 'n', silent = true })

-- Close buffer
vim.api.nvim_set_keymap('', '<D-w>', ':Bdelete<CR>', options)
vim.api.nvim_set_keymap('!', '<D-w>', '<ESC>:Bdelete<CR>', options)
vim.api.nvim_set_keymap('', '<A-w>', ':Bdelete<CR>', options)
vim.api.nvim_set_keymap('!', '<A-w>', '<ESC>:Bdelete<CR>', options)
vim.api.nvim_set_keymap('', '<M-w>', ':Bdelete<CR>', options)
vim.api.nvim_set_keymap('!', '<M-w>', '<ESC>:Bdelete<CR>', options)
-- Magic buffer-picking mode
vim.api.nvim_set_keymap('', '<M-b>', ':BufferPick<CR>', options)
vim.api.nvim_set_keymap('!', '<M-b>', '<ESC>:BufferPick<CR>', options)
vim.api.nvim_set_keymap('', '[0', ':BufferPick<CR>', options)
vim.api.nvim_set_keymap('', ']0', ':BufferPick<CR>', options)
vim.api.nvim_set_keymap('', '[\\', ':BufferPick<CR>', options)
vim.api.nvim_set_keymap('', ']\\', ':BufferPick<CR>', options)

-- Pane navigation integrated with tmux
vim.api.nvim_set_keymap('', '<c-h>', ':TmuxNavigateLeft<cr>', { silent = true })
vim.api.nvim_set_keymap('', '<c-j>', ':TmuxNavigateDown<cr>', { silent = true })
vim.api.nvim_set_keymap('', '<c-k>', ':TmuxNavigateUp<cr>', { silent = true })
vim.api.nvim_set_keymap('', '<c-l>', ':TmuxNavigateRight<cr>', { silent = true })
-- add mapping for :TmuxNavigatePrevious ? c-\, the default, used by toggleterm

-- DAP
vim.keymap.set({ 'n' }, [[<F2>]], [[:lua require('dap').step_into()<CR>]])
vim.keymap.set({ 'n' }, [[<F3>]], [[:lua require('dap').step_over()<CR>]])
vim.keymap.set({ 'n' }, [[<F5>]], [[:lua require('dap').continue()<CR>]])
vim.keymap.set({ 'n' }, [[<F6>]], [[:lua require('dap').step_out()<CR>]])
vim.keymap.set({ 'n' }, [[<F10>]], [[:lua require('dap').toggle_breakpoint()<CR>]])
vim.keymap.set({ 'n' }, [[<F12>]], [[:lua require('dap').step_out()<CR>]])
vim.keymap.set({ 'n' }, [[<Leader>db]], [[:lua require('dap').toggle_breakpoint()<CR>]])
vim.keymap.set({ 'n' }, [[<Leader>dr]], [[:lua require('dap').repl.toggle()<CR>]])
vim.keymap.set({ 'n' }, [[<Leader>dl]], [[:lua require('dap').run_last()<CR>]])

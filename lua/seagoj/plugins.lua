-- This is a mega file. Rather than make each plugin have its own config file,
-- which is how I managed my packer-based nvim config prior to Nix, I'm
-- putting everything in here in sections and themed functions. It just makes it
-- easier for me to quickly update things and it's cleaner when there's
-- interdependencies between plugins. We'll see how it goes.
local M = {}

local signs = require("seagoj.signs")

----------------------- UI --------------------------------
-- Tree, GitSigns, Indent markers, Colorizer, bufferline, lualine, treesitter
M.ui = function()
  -- following options are the default
  -- each of these are documented in `:help nvim-tree.OPTION_NAME`
  -- local nvim_tree_config = require("nvim-tree.config")
  -- local tree_cb = nvim_tree_config.nvim_tree_callback
  require("seagoj.plugins.nvim-tree")

  require("nvim-surround").setup({
    aliases = {
      ["e"] = "**" -- e for emphasis -- bold in markdown
    }
  })

  require("seagoj.plugins.todo-comments")
  require("seagoj.plugins.gitsigns")
  require("diffview").setup {}

  -- if not SimpleUI then require("colorizer").setup({}) end

  require("seagoj.plugins.lualine")
  require("seagoj.plugins.treesitter")
  -- require("seagoj.plugins.bufferline")
  require("seagoj.plugins.dashboard")
end -- UI setup

----------------------- DIAGNOSTICS --------------------------------
M.diagnostics = function()
  vim.diagnostic.config({
    virtual_text = false,
    signs = { active = { signs.signs } },
    update_in_insert = true,
    underline = true,
    severity_sort = true,
    float = {
      focusable = false,
      style = "minimal",
      border = "rounded",
      source = "always",
      header = "",
      prefix = ""
    }
  })
  vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(
    vim.lsp.handlers.hover,
    { border = "rounded" })

  vim.lsp.handlers["textDocument/signatureHelp"] =
      vim.lsp.with(vim.lsp.handlers.signature_help, { border = "rounded" })

  require("trouble").setup {
    group = true, -- group results by file
    icons = true,
    auto_preview = true,
    signs = {
      error = signs.error,
      warning = signs.warn,
      hint = signs.hint,
      information = signs.info,
      other = "﫠"
    }
  }

  require('dapui').setup()
  vim.g.dap_virtual_text = true

  local function attached(client, bufnr)
    local function buf_set_keymap(...)
      vim.api.nvim_buf_set_keymap(bufnr, ...)
    end

    local opts = { noremap = true, silent = false }
    if client.name == "tsserver" or client.name == "jsonls" or client.name ==
        "nil" or client.name == "eslint" or client.name == "html" or
        client.name == "cssls" or client.name == "tailwindcss" then
      -- Most of these are being turned off because prettier handles the use case better
      client.server_capabilities.documentFormattingProvider = false
      client.server_capabilities.documentRangeFormattingProvider = false
    else
      client.server_capabilities.documentFormattingProvider = true
      client.server_capabilities.documentRangeFormattingProvider = true
      require("lsp-format").on_attach(client)
    end

    print("LSP attached " .. client.name)

    vim.api.nvim_buf_set_option(bufnr, "formatexpr",
      "v:lua.vim.lsp.formatexpr()")
    vim.api.nvim_buf_set_option(bufnr, "omnifunc", "v:lua.vim.lsp.omnifunc")
    vim.api.nvim_buf_set_option(bufnr, "tagfunc", "v:lua.vim.lsp.tagfunc")

    local which_key = require("which-key")
    local local_leader_opts = {
      mode = "n",     -- NORMAL mode
      prefix = "<leader>",
      buffer = bufnr, -- Local mappings.
      silent = true,  -- use `silent` when creating keymaps
      noremap = true, -- use `noremap` when creating keymaps
      nowait = true   -- use `nowait` when creating keymaps
    }
    local local_leader_opts_visual = {
      mode = "v",     -- VISUAL mode
      prefix = "<leader>",
      buffer = bufnr, -- Local mappings.
      silent = true,  -- use `silent` when creating keymaps
      noremap = true, -- use `noremap` when creating keymaps
      nowait = true   -- use `nowait` when creating keymaps
    }

    require("symbols-outline").setup({
      keymaps = { close = { "<Esc>", "q", "#7" } }
    })

    local leader_mappings = {
      ["q"] = { "<cmd>TroubleToggle<CR>", "Show Trouble list" },
      l = {
        name = "Local LSP",
        s = { "<cmd>SymbolsOutline<CR>", "Show Symbols" },
        d = {
          "<Cmd>lua vim.lsp.buf.definition()<CR>", "Go to definition"
        },
        D = {
          "<cmd>lua vim.lsp.buf.implementation()<CR>",
          "Implementation"
        },
        i = { "<Cmd>lua vim.lsp.buf.hover()<CR>", "Info hover" },
        I = {
          "<Cmd>Telescope lsp_implementations<CR>", "Implementations"
        },
        r = { "<cmd>Telescope lsp_references<CR>", "References" },
        f = { "<cmd>Telescope code_action<CR>", "Fix Code Actions" },
        t = { "<cmd>lua vim.lsp.buf.signature_help()<CR>", "Signature" },
        e = {
          "<cmd>lua vim.diagnostic.open_float()<CR>",
          "Show Line Diags"
        }
      },
      f = {
        ["sd"] = {
          "<cmd>Telescope lsp_document_symbols<CR>",
          "Find symbol in document"
        },
        ["sw"] = {
          "<cmd>Telescope lsp_workspace_symbols<CR>",
          "Find symbol in workspace"
        }
      }
    }
    which_key.register(leader_mappings, local_leader_opts)
    -- Create a new note after asking for its title.
    buf_set_keymap('', "#7", "<cmd>SymbolsOutline<CR>", opts)
    buf_set_keymap('!', "#7", "<cmd>SymbolsOutline<CR>", opts)
    buf_set_keymap('', 'K', '<cmd>lua vim.lsp.buf.hover()<CR>', opts)
    -- override standard tag jump
    buf_set_keymap('', 'C-]', '<cmd>lua vim.lsp.buf.definition()<CR>', opts)
    buf_set_keymap('!', 'C-]', '<cmd>lua vim.lsp.buf.definition()<CR>', opts)

    -- Set some keybinds conditional on server capabilities
    if client.server_capabilities.document_formatting then
      which_key.register({
        l = {
          ["="] = {
            "<cmd>lua vim.lsp.buf.formatting_sync()<CR>", "Format"
          }
        }
      }, local_leader_opts)
      -- vim.cmd([[
      --       augroup LspFormatting
      --           autocmd! * <buffer>
      --           autocmd BufWritePre <buffer> lua vim.lsp.buf.formatting_sync()
      --       augroup END
      --       ]])
    end
    if client.server_capabilities.implementation then
      which_key.register({
        l = {
          ["I"] = {
            "<cmd>Telescope lsp_implementations<CR>",
            "Implementations"
          }
        }
      }, local_leader_opts)
    end
    if client.server_capabilities.document_range_formatting then
      which_key.register({
        l = {
          ["="] = {
            "<cmd>lua vim.lsp.buf.range_formatting()<CR>",
            "Format Range"
          }
        }
      }, local_leader_opts_visual)
    end
    if client.server_capabilities.rename then
      which_key.register({
        l = { ["R"] = { "<cmd>lua vim.lsp.buf.rename()<CR>", "Rename" } }
      }, local_leader_opts)
    end
  end

  -- LSP stuff - minimal with defaults for now
  local null_ls = require("null-ls")

  -- https://github.com/jose-elias-alvarez/null-ls.nvim/tree/main/lua/null-ls/builtins/formatting
  local formatting = null_ls.builtins.formatting
  -- https://github.com/jose-elias-alvarez/null-ls.nvim/tree/main/lua/null-ls/builtins/diagnostics
  local diagnostics = null_ls.builtins.diagnostics
  local codeactions = null_ls.builtins.code_actions

  require("lsp-format").setup {}

  null_ls.setup {
    debug = false,
    sources = {
      -- formatting.lua_format,
      formatting.alejandra, -- for nix
      formatting.prismaFmt, -- for node prisma db orm
      formatting.prettier.with {

        -- extra_args = {
        --     "--use-tabs", "--single-quote", "--jsx-single-quote"
        -- },
        -- Disable markdown because formatting on save conflicts in weird ways
        -- with the taskwiki (roam-task) stuff.
        filetypes = {
          "javascript", "javascriptreact", "typescript",
          "typescriptreact", "vue", "scss", "less", "html", "css",
          "json", "jsonc", "yaml", "graphql", "handlebars", "svelte"
        },
        disabled_filetypes = { "markdown" }
      }, diagnostics.eslint_d.with {
      args = {
        "-f", "json", "--stdin", "--stdin-filename", "$FILENAME"
      }
    },                                                                -- diagnostics.vale,
      codeactions.eslint_d, codeactions.gitsigns, codeactions.statix, -- for nix
      diagnostics.statix,                                             -- for nix
      null_ls.builtins.hover.dictionary, codeactions.shellcheck,
      diagnostics.shellcheck
      -- removed formatting.rustfmt since rust_analyzer seems to do the same thing
    },
    on_attach = attached
  }
  local lspconfig = require("lspconfig")
  local cmp_nvim_lsp = require("cmp_nvim_lsp")

  local capabilities = vim.tbl_extend('keep', vim.lsp.protocol
    .make_client_capabilities(),
    cmp_nvim_lsp.default_capabilities());

  require('cmp-npm').setup({})
  lspconfig.tsserver.setup { capabilities = capabilities, on_attach = attached }
  lspconfig.lua_ls.setup {
    on_attach = attached,
    capabilities = capabilities,
    filetypes = { "lua" },
    settings = {
      Lua = {
        runtime = {
          -- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
          version = 'LuaJIT'
        },
        diagnostics = {
          -- Get the language server to recognize the `vim` global
          globals = { 'vim', "string", "require" }
        },
        workspace = {
          -- Make the server aware of Neovim runtime files
          library = vim.api.nvim_get_runtime_file("", true),
          checkThirdParty = false
        },
        -- Do not send telemetry data containing a randomized but unique identifier
        telemetry = { enable = false },
        completion = { enable = true, callSnippet = "Replace" }
      }
    }
  }
  lspconfig.svelte.setup { on_attach = attached, capabilities = capabilities }
  lspconfig.tailwindcss.setup {
    on_attach = attached,
    capabilities = capabilities,
    settings = {
      files = { exclude = { "**/.git/**", "**/node_modules/**", "**/*.md" } }
    }
  }
  -- nil_ls is a nix lsp
  lspconfig.nil_ls.setup { on_attach = attached, capabilities = capabilities }
  lspconfig.cssls.setup {
    on_attach = attached,
    capabilities = capabilities,
    settings = { css = { lint = { unknownAtRules = "ignore" } } }
  }
  lspconfig.eslint.setup { on_attach = attached, capabilities = capabilities }
  lspconfig.html.setup { on_attach = attached, capabilities = capabilities }
  lspconfig.bashls.setup { on_attach = attached, capabilities = capabilities }
  -- TODO: investigate nvim-metals and remove line below
  lspconfig.metals.setup { on_attach = attached, capabilities = capabilities } -- for scala
  lspconfig.pylsp.setup { on_attach = attached, capabilities = capabilities }  -- for python
  lspconfig.jsonls.setup {
    on_attach = attached,
    settings = {
      json = {
        validate = { enable = true }
      }
    },
    setup = {
      commands = {
        Format = {
          function()
            vim.lsp.buf.range_formatting({}, { 0, 0 },
              { vim.fn.line "$", 0 })
          end
        }
      }
    },
    capabilities = capabilities
  }
end -- Diagnostics setup

----------------------- TELESCOPE --------------------------------
M.telescope = function()
  local actions = require('telescope.actions')
  local action_state = require('telescope.actions.state')

  local function quicklook_selected_entry(prompt_bufnr)
    local entry = action_state.get_selected_entry()
    -- actions.close(prompt_bufnr)
    vim.cmd("silent !qlmanage -p '" .. entry.value .. "'")
  end

  local function yank_selected_entry(prompt_bufnr)
    local entry = action_state.get_selected_entry()
    actions.close(prompt_bufnr)
    -- Put it in the unnamed buffer and the system clipboard both
    vim.api.nvim_call_function("setreg", { '"', entry.value })
    vim.api.nvim_call_function("setreg", { "*", entry.value })
  end

  local function system_open_selected_entry(prompt_bufnr)
    local entry = action_state.get_selected_entry()
    actions.close(prompt_bufnr)
    os.execute("open '" .. entry.value .. "'")
  end

  require('telescope').setup {
    file_ignore_patterns = {
      "*.bak", ".git/", "node_modules", ".zk/", "Caches/"
    },
    prompt_prefix = SimpleUI and ">" or " ",
    selection_caret = SimpleUI and "↪" or " ",
    -- path_display = { "smart" },
    defaults = {
      path_display = function(_, path)
        local tail = require("telescope.utils").path_tail(path)
        return string.format("%s (%s)", tail,
          require("telescope.utils").path_smart(
            path:gsub("/Users/[^/]*/", "~/"):gsub(
              "/[^/]*$", ""):gsub(
              "/Library/Containers/co.noteplan.NotePlan3/Data/Library/Application Support/co.noteplan.NotePlan3",
              "/NotePlan")))
      end,
      -- path_display = { "truncate" },
      mappings = {
        n = {
          ["<C-y>"] = yank_selected_entry,
          ["<C-o>"] = system_open_selected_entry,
          ["<F10>"] = quicklook_selected_entry,
          ["q"] = require("telescope.actions").close
        },
        i = {
          ["<C-y>"] = yank_selected_entry,
          ["<F10>"] = quicklook_selected_entry,
          ["<C-o>"] = system_open_selected_entry
        }
      },
      vimgrep_arguments = {
        "rg", "--color=never", "--no-heading", "--with-filename",
        "--line-number", "--column", "--smart-case"
      },
      -- Telescope smart history
      history = {
        path = '~/.local/share/nvim/databases/telescope_history.sqlite3',
        limit = 100
      },
      layout_strategy = "flex",
      layout_config = {
        horizontal = { prompt_position = "bottom", preview_width = 0.55 },
        vertical = { mirror = false },
        width = 0.87,
        height = 0.80,
        preview_cutoff = 1
      },
      color_devicons = not SimpleUI,
      set_env = { ["COLORTERM"] = "truecolor" }, -- default = nil,
      file_previewer = require("telescope.previewers").vim_buffer_cat.new,
      grep_previewer = require("telescope.previewers").vim_buffer_vimgrep
          .new,
      qflist_previewer = require("telescope.previewers").vim_buffer_qflist
          .new
    },

    extensions = {
      fzy_native = {
        override_generic_sorter = true,
        override_file_sorter = true
      }
    }
  }
  require 'telescope'.load_extension('fzy_native')
  -- require("telescope").load_extension("zk")
  if vim.fn.has('mac') ~= 1 then
    -- doesn't currently work on mac
    require 'telescope'.load_extension('media_files')
  end
end -- telescope

----------------------- COMPLETIONS --------------------------------
-- cmp, luasnip
M.completions = function()
  require("luasnip/loaders/from_vscode").lazy_load()
  local luasnip = require("luasnip")
  local check_backspace = function()
    local col = vim.fn.col "." - 1
    return col == 0 or
        vim.fn.getline(vim.fn.line(".")):sub(col, col):match "%s"
  end
  local cmp = require 'cmp'
  cmp.setup {
    enabled = function()
      local context = require 'cmp.config.context'
      local buftype = vim.api.nvim_buf_get_option(0, "buftype")
      -- prevent completions in prompts like telescope prompt
      if buftype == "prompt" then return false end
      -- allow completions in command mode
      if vim.api.nvim_get_mode().mode == 'c' then return true end
      -- forbid completions in comments
      return not context.in_treesitter_capture("comment") and
          not context.in_syntax_group("Comment")
    end,
    mapping = {
      ['<C-p>'] = cmp.mapping.select_prev_item(),
      ['<C-n>'] = cmp.mapping.select_next_item(),
      ['<C-d>'] = cmp.mapping.scroll_docs(-4),
      ['<C-f>'] = cmp.mapping.scroll_docs(4),
      ['<C-Space>'] = cmp.mapping.complete({}),
      ['<C-e>'] = cmp.mapping.close(),
      ['<CR>'] = cmp.mapping.confirm {
        behavior = cmp.ConfirmBehavior.Replace,
        select = false
      },
      ["<Tab>"] = cmp.mapping(function(fallback)
        if cmp.visible() then
          cmp.select_next_item()
        elseif luasnip.expandable() then
          luasnip.expand({})
        elseif luasnip.expand_or_jumpable() then
          luasnip.expand_or_jump()
        elseif check_backspace() then
          fallback()
        else
          cmp.mapping.complete({})
          -- fallback()
        end
      end, { "i", "s" }),
      ["<S-Tab>"] = cmp.mapping(function(fallback)
        if cmp.visible() then
          cmp.select_prev_item()
        elseif luasnip.jumpable(-1) then
          luasnip.jump(-1)
        else
          fallback()
        end
      end, { "i", "s" })
    },
    window = { documentation = cmp.config.window.bordered() },
    sources = {
      { name = 'nvim_lsp' }, { name = 'nvim_lsp_signature_help' },
      { name = 'nvim_lua' }, { name = 'emoji' }, { name = 'luasnip' },
      { name = 'path' }, { name = "crates" },
      { name = 'npm',    keyword_length = 3 },
      { name = "buffer", keyword_length = 3 }
    },
    formatting = {
      fields = { "kind", "abbr", "menu" },
      format = function(entry, vim_item)
        -- Kind icons
        vim_item.kind = string.format("%s",
          signs.kind_icons[vim_item.kind])
        vim_item.menu = ({
          nvim_lsp = "[LSP]",
          nvim_lsp_signature_help = "[LSPS]",
          luasnip = "[Snippet]",
          buffer = "[Buffer]",
          path = "[Path]"
        })[entry.source.name]
        return vim_item
      end
    },
    snippet = { expand = function(args) luasnip.lsp_expand(args.body) end }
  }
  cmp.setup.cmdline('/', {
    mapping = cmp.mapping.preset.cmdline(),
    sources = { { name = 'buffer' } }
  })
  cmp.setup.cmdline(':', {
    mapping = cmp.mapping.preset.cmdline(),
    sources = cmp.config.sources({ { name = 'path' } }, {
      { name = 'cmdline', option = { ignore_cmds = { 'Man', '!' } } }
    })
  })
end -- completions

----------------------- NOTES --------------------------------
-- zk (zettelkasten lsp), taskwiki, focus mode, grammar
M.notes = function()
  -- Grammar
  vim.g["grammarous#disabled_rules"] = {
    ['*'] = {
      'WHITESPACE_RULE', 'EN_QUOTES', 'ARROWS', 'SENTENCE_WHITESPACE',
      'WORD_CONTAINS_UNDERSCORE', 'COMMA_PARENTHESIS_WHITESPACE',
      'EN_UNPAIRED_BRACKETS', 'UPPERCASE_SENTENCE_START',
      'ENGLISH_WORD_REPEAT_BEGINNING_RULE', 'DASH_RULE', 'PLUS_MINUS',
      'PUNCTUATION_PARAGRAPH_END', 'MULTIPLICATION_SIGN', 'PRP_CHECKOUT',
      'CAN_CHECKOUT', 'SOME_OF_THE', 'DOUBLE_PUNCTUATION', 'HELL',
      'CURRENCY', 'POSSESSIVE_APOSTROPHE', 'ENGLISH_WORD_REPEAT_RULE',
      'NON_STANDARD_WORD'
    }
  }
  -- Grammar stuff
  vim.cmd(
    [[command StartGrammar2 lua require('seagoj.plugins').grammar_check()]])
end -- notes

M.grammar_check = function()
  vim.cmd('packadd vim-grammarous')
  local opts = { noremap = false, silent = true }
  local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(0, ...) end

  buf_set_keymap('', '<leader>gf', '<Plug>(grammarous-fixit)', opts)
  buf_set_keymap('', '<leader>gx', '<Plug>(grammarous-remove-error)', opts)
  buf_set_keymap('', ']g', '<Plug>(grammarous-move-to-next-error)', opts)
  buf_set_keymap('', '[g', '<Plug>(grammarous-move-to-previous-error)', opts)
  vim.cmd('GrammarousCheck')
end

----------------------- MISC --------------------------------
-- rooter, kommentary, autopairs, tmux, toggleterm
M.misc = function()
  vim.g.lf_map_keys = 0 -- lf.vim disable default keymapping

  -- Change project directory using local cd only
  -- vim.g.rooter_cd_cmd = 'lcd'
  -- Look for these files/dirs as hints
  -- vim.g.rooter_patterns = {
  --     '.git', '_darcs', '.hg', '.bzr', '.svn', 'Makefile', 'package.json',
  --     '.zk', 'Cargo.toml', 'build.sbt', 'Package.swift', 'Makefile.in'
  -- }
  require('project_nvim').setup({
    active = true,
    on_config_done = nil,
    manual_mode = false,
    detection_methods = { "pattern", "lsp" },
    patterns = {
      ".git", "_darcs", ".hg", ".bzr", ".svn", "Makefile", "package.json",
      ".zk", "build.sbt", "Package.swift", "Makefile.in", "README.md",
      "flake.nix"
    },
    show_hidden = false,
    silent_chdir = true,
    ignore_lsp = {}
  })
  require('telescope').load_extension('projects')

  require('Comment').setup()
  require("which-key").register({
    ["<leader>c<space>"] = {
      '<Plug>(comment_toggle_linewise_current)', "Toggle comments"
    },
    ["g/"] = { '<Plug>(comment_toggle_linewise_current)', "Toggle comments" }
  }, { mode = "n", silent = true, norewrap = true })
  require("which-key").register({
    ["<leader>c<space>"] = {
      '<Plug>(comment_toggle_linewise_visual)', "Toggle comments"
    },
    ["g/"] = { '<Plug>(comment_toggle_linewise_visual)', "Toggle comments" }
  }, { mode = "v", silent = true, norewrap = true })

  require('nvim-autopairs').setup({})

  vim.g.tmux_navigator_no_mappings = 1
  vim.g.tmux_navigator_disable_when_zoomed = 1

  require("toggleterm").setup {
    open_mapping = [[<c-\>]],
    insert_mappings = true, -- from normal or insert mode
    start_in_insert = true,
    hide_numbers = true,
    direction = 'vertical',
    size = function(_) return vim.o.columns * 0.3 end,
    close_on_exit = true
  }
  vim.api.nvim_set_keymap('t', [[<C-\]], "<Cmd>ToggleTermToggleAll<cr>",
    { noremap = true })

  local Terminal = require('toggleterm.terminal').Terminal

  -- lazygit
  local lazygit = Terminal:new({
    cmd = "lazygit",
    dir = "git_dir",
    hidden = true,
    direction = "float",
    float_opts = {
      border = "double",
    },
    -- function to run on opening the terminal
    on_open = function(term)
      vim.cmd("startinsert!")
      vim.keymap.set({ 'n' }, 'q', '<CMD>close<CR>', { noremap = true, silent = true, buffer = term.bufnr })
    end,
    -- function to run on closing the terminal
    on_close = function(term)
      vim.cmd("startinsert!")
    end,
  })

  function _lazygit_toggle()
    lazygit:toggle()
  end

  vim.keymap.set({ 'n' }, '<leader>g', _lazygit_toggle, { noremap = true, silent = true })

  -- mergetool
  -- require('git-conflict').setup({
  --   default_mappings = true,     -- disable buffer local mapping created by this plugin
  --   disable_diagnostics = false, -- This will disable the diagnostics in a buffer whilst it is conflicted
  --   highlights = {
  --     -- They must have background color, otherwise the default color will be used
  --     incoming = 'DiffText',
  --     current = 'DiffAdd',
  --   }
  -- })
end -- misc

return M
